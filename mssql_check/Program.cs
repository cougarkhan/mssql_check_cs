﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;
using System.IO;

namespace mssql_check
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] code = { "INFO", "WARNING", "ERROR" };
            string settings_file = args[0];
            Settings config = new Settings(settings_file);
            Mssql mssql = config.server.mssql;
            bool result = new Boolean();


            ProgramFunctions pf = new ProgramFunctions(config.server.debug.flag);

            SqlConnection mssql_conn = pf.GetMSSqlConnection(mssql);
            if (mssql_conn == null)
            {
                Console.WriteLine("ERROR: MSSQL Connection Object could not be created.");
                Environment.Exit(2);
            }
            SqlCommand mssql_cmd = pf.GetMSSqlCommand(mssql_conn);
            if (mssql_cmd == null)
            {
                Console.WriteLine("ERROR: MSSQL Command Object could not be created.");
                Environment.Exit(2);
            }
            mssql_conn = pf.OpenMSSqlConnection(mssql_conn);
            if (mssql_conn == null)
            {
                Console.WriteLine("ERROR: MSSQL Connection could not be opened.");
                Environment.Exit(2);
            }
            if (mssql_conn != null && mssql_conn.State == ConnectionState.Open)
            {
                result = pf.ExecuteMSSqlQuery(mssql_cmd, mssql);
                mssql_conn = pf.CloseMSSqlConnection(mssql_conn);
                if (result)
                {

                    Console.WriteLine("MSSQL SQL Query Succeeded.");
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine("ERROR: MSSQL SQL Query Failed.");
                    Environment.Exit(2);
                }
            }

            if (config.server.debug.flag)
            {
                Console.ReadLine();
            }
        }
    }
}
