﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;

namespace mssql_check
{
    class Settings
    {
        public RootObject server { get; set; }
        
        public Settings(string path_to_config_file)
        {
            try
            {
                JavaScriptSerializer ser = new JavaScriptSerializer();
                string json = File.ReadAllText(path_to_config_file);
                List<RootObject> configs = ser.Deserialize<List<RootObject>>(json);
                server = configs[0];
                
            }
            catch (Exception e)
            {
            }
        }
    }

    public class Mssql
    {
        public string instance { get; set; }
        public string database { get; set; }
        public string sql_query { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public bool trusted_conn { get; set; }
        public string password_phrase = "fjlsdjflhsgkjdfsgkdfsgk";
    }

    public class Debug
    {
        public bool flag { get; set; }
    }

    public class RootObject
    {
        public Mssql mssql { get; set; }
        public Debug debug { get; set; }
    }
}
